# Rocketchat Chat Alert Action

This app allows users to send Splunk alerts to a Rocketchat channel.

If you need any support or want to report any problem please create an issue on https://gitlab.com/dakiri/rocketchat_alert_action/-/issues