#!/bin/python

import requests
from requests import sessions
import sys
import json
#from rocketchat_API.rocketchat import RocketChat
import sys, os, datetime
import re

class RocketChat:
    API_path = '/api/v1/'

    def __init__(self, user=None, password=None, auth_token=None, user_id=None,
                 server_url='http://127.0.0.1:3000', ssl_verify=True, proxies=None,
                 timeout=30, session=None):
        self.headers = {}
        self.server_url = server_url
        self.proxies = proxies
        self.ssl_verify = ssl_verify
        self.timeout = timeout
        self.req = session or requests
        if user and password:
            self.login(user, password)  # skipcq: PTC-W1006
        if auth_token and user_id:
            self.headers['X-Auth-Token'] = auth_token
            self.headers['X-User-Id'] = user_id

    @staticmethod
    def __reduce_kwargs(kwargs):
        if 'kwargs' in kwargs:
            for arg in kwargs['kwargs'].keys():
                kwargs[arg] = kwargs['kwargs'][arg]

            del kwargs['kwargs']
        return kwargs

    def __call_api_post(self, method, files=None, use_json=True, **kwargs):
        reduced_args = self.__reduce_kwargs(kwargs)
        # Since pass is a reserved word in Python it has to be injected on the request dict
        # Some methods use pass (users.register) and others password (users.create)
        if 'password' in reduced_args and method != 'users.create':
            reduced_args['pass'] = reduced_args['password']
        if use_json:
            return self.req.post(self.server_url + self.API_path + method,
                                 json=reduced_args,
                                 files=files,
                                 headers=self.headers,
                                 verify=self.ssl_verify,
                                 proxies=self.proxies,
                                 timeout=self.timeout
                                 )
        else:
            return self.req.post(self.server_url + self.API_path + method,
                                 data=reduced_args,
                                 files=files,
                                 headers=self.headers,
                                 verify=self.ssl_verify,
                                 proxies=self.proxies,
                                 timeout=self.timeout
                                 )

    def login(self, user, password):
        request_data = {
            'password': password
        }
        if re.match(r'^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', user):
            request_data['user'] = user
        else:
            request_data['username'] = user
        login_request = requests.post(self.server_url + self.API_path + 'login',
                                      data=request_data,
                                      verify=self.ssl_verify,
                                      proxies=self.proxies)
        if login_request.status_code == 401:
            raise RocketAuthenticationException()

        if login_request.status_code == 200:
            if login_request.json().get('status') == "success":
                self.headers['X-Auth-Token'] = login_request.json().get('data').get('authToken')
                self.headers['X-User-Id'] = login_request.json().get('data').get('userId')
                return login_request
            else:
                raise RocketAuthenticationException()
        else:
            raise RocketConnectionException()

    def logout(self, **kwargs):
        return self.__call_api_post('logout', kwargs=kwargs)

    def chat_post_message(self, text, room_id=None, channel=None, **kwargs):
        if room_id:
            return self.__call_api_post('chat.postMessage', roomId=room_id, text=text, kwargs=kwargs)
        elif channel:
            return self.__call_api_post('chat.postMessage', channel=channel, text=text, kwargs=kwargs)
        else:
            raise RocketMissingParamException('roomId or channel required')

def log(msg):
    f = open(os.path.join(os.environ["SPLUNK_HOME"], "var", "log", "splunk", "rocketchat_alert.log"), "a")
    print(str(datetime.datetime.now().isoformat()), msg, file=f)
    f.close()

def send_alert(message, bot_login, bot_password,bot_alias, rocketchat_url,rocketchat_channel,debug):

    with sessions.Session() as session:
            session.verify = True
            rocket = RocketChat(bot_login,bot_password,server_url=rocketchat_url,session=session)

    result=rocket.chat_post_message(message, channel=rocketchat_channel, alias=bot_alias).json()

def main():
    payload = json.loads(sys.stdin.read())

    config = payload.get('configuration', dict())
    debug =config.get('debug',0)
    if debug:
        log("got arguments %s" % sys.argv)
        log("got payload: %s" % payload)

    message = config.get('message')
    result_link = config.get('result_link')

    bot_login = config.get('bot_login')
    bot_password = config.get('bot_password')
    rocketchat_url = config.get('rocketchat_url')
    rocketchat_channel= config.get('rocketchat_channel')
    bot_alias=config.get('bot_alias')
    display_raw=config.get('display_raw')

    if display_raw == "1":
        result = payload.get('result')
        raw    = result.get('_raw')
        message = "%s\n```%s```"%(message,raw)

    if result_link == "1":
        link = str(payload.get('results_link'))
        message = "%s\n[link to result](%s)"%(message,link)

    send_alert(message, bot_login, bot_password, bot_alias, rocketchat_url,rocketchat_channel,debug)

if __name__ == "__main__":
    main()
